// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { path, app, newFollow, unfollow } = context;
    if(newFollow){
      app.service(path).get(newFollow)
      .then(followed => {
        followed.followers.push(context.params.user._id); 
        app.service(path).patch(newFollow, {followers: followed.followers}, context.params); 
      });
      app.service(path).emit(
        'newFollow',
        {
          newFollow, 
          from: {
            _id: context.params.user._id, 
            name: context.params.user.name
          }
      });  
      app.service(path).emit('followed', context.params.user._id); 
    } else if(unfollow){
      app.service(path).get(unfollow)
      .then(unfollowed => {
        const updatedFollowersList = unfollowed.followers.filter((follower) => {
          return follower !== context.params.user._id
        });
        app.service(path).patch(unfollow, {followers: updatedFollowersList}, context.params);
        app.service(path).emit('unfollow', context.params.user._id); 
      });
    }
    return context;
  };
};
