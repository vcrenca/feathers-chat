// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {

    const {app, params, method, result} = context;
  
    const addUserInMessage = async (message) => {
      const user = await app.service('users').get(message.senderId, params); 
      delete message.senderId
      return{
        sender: user,
        ...message
      };
    };

    if(method === 'find'){
      context.result.data = await Promise.all(result.data.map(addUserInMessage)); 
    } else {
      context.result = await addUserInMessage(result); 
    }

    return context;
  };
};
